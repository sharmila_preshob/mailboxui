import React from 'react';
import './App.css';
import  Email  from './components/email';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      selectedEmail: '',
      openedMail: '',
      clicked: [],
      loading: true
    }
  }
  componentDidMount() {
      fetch('../data.json')
         .then(response => response.json())
         .then(data => this.setState({ messages: data, selectedEmail: data[0].uid, loading: false}));
    }
  selectedMail = (ee) => {
    this.setState({selectedEmail: ee.target.id});
  }
  handleDelete = (e, id) => {
      let index = this.state.messages.findIndex(e=>e.uid===id);
      let newMails = this.state.messages.slice();
      newMails.splice(index, 1);
      this.setState({ messages: newMails });
  }
  handleOpenedMail = (clickedMsg) => {
    this.state.clicked.push(clickedMsg);
    this.setState({clicked:this.state.clicked});
  }
  showDate = (date) => {
    let newDate = new Date(date * 1000).toString().slice(4, 15);
    let newTime = new Date(date * 1000).toISOString().slice(11, 19);
    return (newDate + ' / ' + newTime);
  }

  render () {
    const msgList = this.state.messages.map((msg)=> {
      return <li id={msg.uid} key={msg.uid}
      className={"mailbox-list-item " + (this.state.clicked.includes(msg) ? "read" : "unread")}
      onClick={this.selectedMail}>
      <div className="mailbox-list-item-sender">{msg.sender} </div>
      <div className="mailbox-list-item-msg">{msg.message}</div>
      <div className="mailbox-list-item-date">{this.showDate(msg.time_sent)}</div>
      <div className="mailbox-list-delete" onClick={e=>this.handleDelete(e, msg.uid)}>
        <img src={`${process.env.PUBLIC_URL}/assets/images/delete.svg`} alt="delete"/>
      </div>
      </li>;
    });
    return (
      (this.state.loading === true) ? (
        <div className="loading"></div>
      ):
      (<div className="mailbox">
          <div className="mailbox-list">
          {
            (this.state.messages).length < 1 ? (<div className="mailbox-empty">No new messages recieved</div>) :
            (msgList)
          }
          </div>
          <div className="mailbox-messge">
              <Email selectedEmail={this.state.selectedEmail} dateTime={this.showDate}
              msg={this.state.messages} openedMail={this.handleOpenedMail}/>
          </div>
      </div>)
    );
  }
  }


export default App;
