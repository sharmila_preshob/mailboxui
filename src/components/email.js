import React from 'react';


class Email extends React.Component {
  render() {
    const mailSelected= this.props.selectedEmail;
    //console.log(this.props.msg[0]);
    if (mailSelected) {
      var mail = this.props.msg.filter(function(mail) {
       return mail.uid === mailSelected;
     })[0];

      var selected_mailbox = <div>
      <div className="message-details">
        <div className="message-sender">From: {mail.sender}</div>
        <div className="message-date">{this.props.dateTime(mail.time_sent)}</div>
      </div>
      <div className="message-content">{mail.message}</div>
      <button className="message-read-btn" onClick={e => this.props.openedMail(mail)}>Mark as read</button>
      </div>;
    } else {
      selected_mailbox = <div className="message-deleted"> Message got deleted</div>;
    }
    return (
      <div>
       {selected_mailbox}
      </div>
    );
  }
}

export default Email;
