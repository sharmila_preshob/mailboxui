The project is done using React. To run the project please do the following

### npm install (to install all dependencies)
### npm start (it will open in the default browser)

-------------------------------------------------------

Description:

List of mails are displayed on the left with sender name, few lines from the message and the date/time recieved. When you click on one of the lists, the full content is displayed on the right with all the details. Also there is an option to mark the message as read. Once clicked the read message in the list on the left is not highlighted anymore. There is option to delete the messsage. Once deleted, it will show a message "Message got deleted". And when all the messages are deleted, it displays a message "No new messages received".

